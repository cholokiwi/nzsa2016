# README #

Talk, poster and data presented at the 2016 NZSA, Auckland, New Zealand. See abstracts below:

Talk

**Bayes meets baby poo: Bayesian estimation of infant gut microbial diversity using Metagenomic data**

Metagenomics is the study of microbial communities using samples found in the physical environment. Through the sequencing of whole genomes, it aims to identify microbial species and their functions within their natural environment.

In this talk we identify variant strains, ie dominant genotypes, of Bacteroides Faecis (B. faecis) using shotgun metagenomic data from baby stool samples. Overall, the data comprises an ordinal response (“equal to reference site”, “segregating site”, and “variant site”) for 1992 sites measured over 25 occasions for one infant. We use a finite mixture model that includes random effects by strain and occasion and estimate the model parameters using a Bayesian approach. Model comparison is carried out using the Widely Applicable Information Criterion (WAIC). The results show that there are at least three strains of B. faecis due to different levels of variation across occasions. Although using an ordinal response instead of counts, these results agree with a previous analysis of this dataset.



Poster

**Expectation–maximization of baby poo: Frequentist estimation of infant gut microbial diversity using Metagenomic data**

Metagenomics is the study of microbial communities using samples found in the physical environment. Through the sequencing of whole genomes, it aims to identify microbial species and their functions within their natural environment.
In this poster we identify variant strains, ie dominant genotypes, of Bacteroides Faecis (B. faecis) using shotgun metagenomic data from baby stool samples. Overall, the data comprises an ordinal response (“equal to reference site”, “segregating site”, and “variant site”) for 1992 sites measured over 25 occasions for one infant. We use a finite mixture model that includes random effects by strain and occasion and estimate the model parameters using the EM algorithm. Model comparison is carried out using several information criteria: the AIC, BIC and ICL. The results show that there are at least three strains of B. faecis due to different levels of variation  across occasions. Although using an ordinal response instead of counts, these results agree with a previous analysis of this dataset.


### Comments/questions to ###
Roy Costilla

PhD Statistics

Institute for Molecular Biosciences. Univerity of Queensland

r.costilla@imb.uq.edu.au

https://www.researchgate.net/profile/Roy_Costilla

https://twitter.com/CmRoycostilla
